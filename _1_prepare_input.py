import os
import sys
import requests
import json
import bibtexparser
import time

from bs4 import BeautifulSoup
from selenium import webdriver
from unidecode import unidecode

import _configuration as cfg

# --------------------------------------------------
# Configuration

input_file       = cfg.input_file
output_file      = cfg.output_file
list_from_CROSBI = cfg.list_from_CROSBI
authors          = cfg.authors
journals         = cfg.journals
issn             = cfg.issn
keywords         = cfg.keywords

# --------------------------------------------------

def get_list_of_papers(list_of_papers):

  with open(list_of_papers) as f:
    temp = f.read()

  return bibtexparser.loads(temp)

def get_url(browser, doi, wait=False):

  browser.get('https://dx.doi.org/' + doi)

  if wait:
    time.sleep(30) # pause to deal with the initial Captcha bot check (needs to be done manually)

  url = browser.current_url

  return url

def download_pdf(browser, paper):

  print('Downloading pdf...')

  if not os.path.isdir('pdf'):
    os.mkdir('pdf')

  _pdf = 'pdf/' + (paper['eprint'] if 'eprint' in paper else paper['doi'].replace('/','_')) + '.pdf'

  if os.path.isfile(_pdf):
    print('%s already exists' % _pdf)
    return _pdf

  url = ''
  if 'eprint' in paper:
    url = 'http://arxiv.org/' + _pdf
  else:
    url = browser.find_element_by_css_selector("a[href$='pdf']").get_attribute('href')

  page = requests.get(url)
  file = open( _pdf, 'wb')
  file.write(page.content)
  file.close()

  print('Downloaded %s' % _pdf)
  return _pdf

def get_abstract_and_title(arxiv_name):

  r = requests.get('http://arxiv.org/abs/' + arxiv_name)

  soup = BeautifulSoup(r.content, 'html.parser')

  abstract = ''
  title = ''

  for line in soup.find_all('blockquote', {'class':'abstract mathjax'}):
    abstract += line.text.strip('\n')[len('Abstract:'):].strip()

  for line in soup.find_all('h1', {'class':'title mathjax'}):
    title += line.text[len('Title:'):].strip()

  return [abstract, title.replace('  ', ' ') ]

def get_journal(name, volume):
  if name == 'Eur. Phys. J.' or name == 'Phys. Lett.' or name =='Phys. Rev.':
    name = name + ' ' + volume[0]

  if name not in journals.keys():
    return "*** Unknown journal '" + name + "' encountered! Please put it in the list of known journals or remove this article from the input list. ***"

  return journals[name]

def get_issn(name, volume):
  if name == 'Eur. Phys. J.' or name == 'Phys. Lett.' or name == 'Phys. Rev.':
    name = name + ' ' + volume[0]

  if name not in issn.keys():
    return ['', '']

  return issn[name]

def get_volume(name, volume):
  if name == 'Eur. Phys. J.' or name == 'Phys. Lett.' or name == 'Phys. Rev.':
    return volume[1:]
  else:
    return volume

def prepare_input_for_CROSBI(list_of_papers, list_of_DOIs):

  browser = webdriver.Firefox()

  data = {}
  error = {}

  counter = 0
  counter_skip = 0

  # Loop over all papers which are stored in bib
  for _n, _p in enumerate(list_of_papers.entries, 1):

    print('------------------------------------------------')
    print('Paper:', _n)

    # DOI
    _doi = _p['doi']
    print('\nDOI:', _doi)

    if _doi.lower() in list_of_DOIs:
      print('  Paper already in CROSBI')
      continue
    else:
      print('  Paper not in CROSBI')

    # url
    try:
      _url = get_url(browser, _doi, _n==1).rstrip()
    except Exception as e:
      print(e)
      break

    print(_url)

    # Download pdf if it doesn't exist already
    try:
      _pdf = download_pdf(browser, _p)
    except Exception as e:
      print(e)
      break

    # Get the arXiv paper id (if defined)
    _eprint = (_p['eprint'] if 'eprint' in _p else '')

    # Fetch paper data from INSPIRE in JSON format
    # (more info at https://github.com/inspirehep/rest-api-doc)
    url = 'https://inspirehep.net/api/doi/%s'%(_doi)
    paper_data = requests.get(url).json()

    # Get abstract and title from arXiv or INSPIRE since they don't have BibTeX syntax
    _abstract = ''
    _title = ''
    if 'eprint' in _p:
      _abstract, _title = get_abstract_and_title(_eprint)
    else:
      _abstract = paper_data['metadata']['abstracts'][0]['value'].strip()
      _title = paper_data['metadata']['titles'][0]['title'].strip()

    # Authors
    all_authors = paper_data['metadata']['authors']

    authors_pretty = []

    # All authors
    for author in all_authors:
        # Cro authors
        for _a, _a_pretty in authors.items():

            author_text = author['full_name']
            if _a in unidecode(author_text):
                authors_pretty.append(_a_pretty)
                break

    # First author
    _authors = all_authors[0]['full_name'] + '; ...'

    # Ordered Cro authors
    authors_pretty.sort()
    for _a_pretty in authors_pretty:
        _authors += ' ; ' + _a_pretty

    # Last author
    _authors += ' ; ... ; ' + all_authors[len(all_authors)-1]['full_name']

    # Year
    _year = _p['year']

    # Journal
    _journal = get_journal( _p['journal'], _p['volume'])

    # ISSN
    _issn = get_issn( _p['journal'], _p['volume'])

    # Volume
    _volume = get_volume( _p['journal'], _p['volume'])

    # Number
    _number = (_p['number'] if 'number' in _p else '')

    # Pages
    _page_first = ''
    _page_last = ''
    _page_tot = ''
    pages = _p['pages']

    # Article number
    _article_no = ''

    # quite often the pages field corresponds to the article number (DOI string also often ends with it), not the page numbers range
    # if we have a page numbers range
    if '-' in pages:
        _pages = pages.split('-')
        _page_first = _pages[0]
        _page_last = _pages[1]
    else:
        _article_no = pages
        _page_tot = str(paper_data['metadata']['number_of_pages'])

    # Keywords
    keywords_length = 0
    keywords_lower = []
    for k in keywords:
        if (keywords_length + len(k) + 2) < 480:
            keywords_length += (len(k) + 2)
            keywords_lower.append(k.strip().lower())
        else:
            break

    for k in paper_data['metadata']['keywords']:
        k_text = k['value'].strip()
        if not k_text.lower() in keywords_lower:
            if (keywords_length + len(k_text) + 2) < 480:
                keywords_length += (len(k_text) + 2)
                keywords.append(k_text)
            else:
                break

    _keywords = '; '.join(keywords)

    # Save output
    _temp = {}
    _temp['doi']             = _doi
    _temp['eprint']          = _eprint
    _temp['autori']          = _authors
    _temp['naslov']          = _title
    _temp['godina']          = _year
    _temp['casopis']         = _journal
    _temp['issn']            = _issn[0]
    _temp['e-issn']          = _issn[1]
    _temp['volumen']         = _volume
    _temp['broj']            = _number
    _temp['stranica_prva']   = _page_first
    _temp['stranica_zadnja'] = _page_last
    _temp['broj_rada']       = _article_no
    _temp['ukupno_stranica'] = _page_tot
    _temp['sazetak']         = _abstract
    _temp['kljucne_rijeci']  = _keywords
    _temp['url']             = _url
    _temp['file']            = _pdf

    if 'Unknown journal' in _journal:
      error[_doi] = _temp
      counter_skip += 1
    else:
      data[_doi] = _temp
      counter += 1


    print('\narXiv:', (_eprint if _eprint != '' else 'N/A'))
    print('Title:', _title)
    print('Authors:', _authors)
    print('Year:', _year)
    print('Journal:',_journal)
    print('ISSN:',_issn[0])
    print('e-ISSN:',_issn[1])
    print('Vol.:',_volume)
    print('Number:', (_number if _number != '' else 'N/A'))
    print('First page:', (_page_first if _page_first != '' else 'N/A'))
    print('Last page:', (_page_last if _page_last != '' else 'N/A'))
    print('Article no.:', (_article_no if _article_no != '' else 'N/A'))
    print('Pages:', (_page_tot if _page_tot != '' else 'N/A'))
    print('\nAbstract:', _abstract)
    print('\nKeywords:', _keywords)
    print('URL:', _url)
    print('File:', _pdf, '\n')

  print('------------------------------------------------')

  print('\n%i papers prepared for upload\n' % counter)
  if counter_skip > 0:
    print('%i papers skipped\n' % counter_skip)

  browser.quit()

  # Output file, i.e. input for CROSBI
  with open(output_file, 'w') as outfile:
    json.dump(data, outfile)

  if len(error) > 0:
    with open(output_file.rstrip('.json')+'_error.json', 'w') as outfile:
      json.dump(error, outfile)

# --------------------------------------------------

if __name__ == '__main__':

  # Load list of papers from bib file
  list_of_papers = get_list_of_papers(input_file)

  # Get list of DOIs for already uploaded papers
  list_of_papers_from_CROSBI = get_list_of_papers(list_from_CROSBI)
  list_of_CROSBI_DOIs = []

  for _p in list_of_papers_from_CROSBI.entries:
    list_of_CROSBI_DOIs.append( _p['doi'].lower() )

  # Create input for CROSBI, name of output hardcoded in the function
  prepare_input_for_CROSBI(list_of_papers, list_of_CROSBI_DOIs)
