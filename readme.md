# CROSBI tool

A tool for uploading articles to CROSBI database. The instructions below were tested on Ubuntu 22.04 but might also work on other Debian-based Linux distributions.

## Getting started

### Installation

```
virtualenv -p python3.10 crosbi-env
source crosbi-env/bin/activate
git clone https://gitlab.cern.ch/CMS-IRB/crosbi.git
cd crosbi
pip install -r requirements.txt
sudo apt install firefox-geckodriver
```

The next time you will need to work with the repository, you will just need to source the environment

```
source crosbi-env/bin/activate
cd crosbi
```

## Running the code

### Getting the list of articles
1. Go to http://inspirehep.net

2. Get the list of articles by using the following command (2022 is used here just as an example)
```
find a brigljevic, v and cn cms and jy 2022 and ps p
```
**NOTE:** Change the author (`a`), the collaboration name (`cn`) and the journal year (`jy`) fields as appropriate.

Place the pointer above the "cite all" button and select BibTeX in the drop-down list as the output format. Save the output to `list_of_papers.bib`

Please note that the returned list of articles could contain errata published in 2022 for otherwise older articles. Please remove any articles for which the `year` field is older than 2022. In addition, check if there are any articles with more than one DOI string. These are typically the already-mentioned errata articles but it is nevertheless good to double-check the reason for more than one DOI string. Either way, you will need to decide how to handle such articles, exclude them altogether or keep them but with just one DOI string (we generally do not want to upload errata articles).

3. Download a list of articles already uploaded to CROSBI to avoid uploading duplicates
```
wget -O list_from_CROSBI.bib "https://www.bib.irb.hr/pretraga?operators=and|Brigljević, Vuko (259320)|text|author&yearRange=2017|2022&export=bibtex"
```
**NOTE:** Change `Brigljević, Vuko (259320)` and (`2017|2022`) to an appropriate author and year range, respectively.

4. Run the following command
```
python -u _1_prepare_input.py |& tee 1_prepare_input_2022.log
```
**NOTE:** Before running the script, please check that the [keywords](https://gitlab.cern.ch/CMS-IRB/crosbi/blob/master/_configuration.py#L8), the [collaboration name](https://gitlab.cern.ch/CMS-IRB/crosbi/blob/master/_configuration.py#L9) and the [author names](https://gitlab.cern.ch/CMS-IRB/crosbi/blob/master/_configuration.py#L11) are correctly defined in [`_configuration.py`](https://gitlab.cern.ch/CMS-IRB/crosbi/blob/master/_configuration.py)

`_1_prepare_input.py` takes `list_of_papers.bib` and `list_from_CROSBI.bib` as inputs and collects all the information for CROSBI and stores it in the `list_of_CROSBI_input.json` file. Additionaly, all articles are downloaded as pdfs and saved to the `pdf/` subdirectory.


### Uploading to CROSBI
1. Before uploading, change username and password in [`_configuration.py`](https://gitlab.cern.ch/CMS-IRB/crosbi/blob/master/_configuration.py#L5-6)

2. Start uploading
```
python -u _2_upload_files.py |& tee 2_upload_files_2022.log
```
`_2_upload_files.py` takes `list_of_CROSBI_input.json` and `list_from_CROSBI.bib` as inputs and uploads all the collected information and pdfs to CROSBI. Note that `list_from_CROSBI.bib` as input is a leftover from an earlier version of `_2_upload_files.py` when duplicate checking was done during the upload step. It is, therefore, technically redundant here because it is already used during the information collection. Nevertheless, it was left here as an extra sanity check.

Please note that `_2_upload_files.py` has some extra command-line options available

```
python _2_upload_files.py -h
```
listed below
```
Usage: python _2_upload_files.py [options]
Example: python _2_upload_files.py -n

Options:
  -h, --help            show this help message and exit
  -n, --dry_run         Perform a dry run with no uploads made
  -p PAPERS, --papers=PAPERS
                        Comma-separated list of papers or paper ranges
```
The dry run `-n` (`--dry_run`) option does everything except the final upload step and is, therefore, useful for checking the paper info being filled into the web form but without uploading anything to CROSBI. In addition, the  `-p` (`--papers`) option allows to restrict the upload or dry-run checking to a selected list of papers or paper ranges, e.g. `-p 1,3,5-8,10`, or just one paper, e.g. `-p 7`. This option is particularly useful for resuming failed uploads. For example, if the upload fails on paper `N`, after investigating and fixing possible problems, the upload can be resumed like this
```
python _2_upload_files.py -p N-
```

3. After uploading is done, send `uploaded_DOIs.txt` to Bojan Macan (Bojan.Macan@irb.hr) and Alen Vodopijevec (alen@irb.hr) who can check for possible problems or needed fixes on the CROSBI side.
