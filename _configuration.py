# --------------------------------------------------
# Configuration
# --------------------------------------------------

# placeholders for AAI@EduHr username and password (need to be filled in before uploading to CROSBI)
username            = 'username@irb.hr'
password            = 'password'

keywords            = ['High energy physics', 'Experimental particle physics', 'LHC', 'CMS']
collaboration_name  = 'CMS Collaboration'

authors = {
  'Godinovic, N'    : 'Godinović, Nikola',
  'Lelas, D'        : 'Lelas, Damir',
  'Puljak, I'       : 'Puljak, Ivica',
  'Antunovic, Z'    : 'Antunović, Željko',
  'Kovac, M'        : 'Kovač, Marko',
  'Brigljevic, V'   : 'Brigljević, Vuko',
  'Kadija, K'       : 'Kadija, Krešo',
  'Luetic, J'       : 'Luetić, Jelena',
  'Micanovic, S'    : 'Mićanović, Saša',
  'Sudic, L'        : 'Sudić, Lucija',
  'Susa, T'         : 'Šuša, Tatjana',
  'Polic, D'        : 'Polić, Dunja',
  'Morovic, S'      : 'Morović, Srećko',
  'Tikvica, L'      : 'Tikvica, Lucija',
  'Mekterovic, D'   : 'Mekterović, Darko',
  'Ferencek, D'     : 'Ferenček, Dinko',
  'Starodumov, A'   : 'Starodumov, Andrey',  # Andrey's name written as 'Andrei' in the CMS database
  'Mesic, B'        : 'Mesić, Benjamin',
  'Duric, S'        : 'Đurić, Senka',
  'Roguljic, M'     : 'Roguljić, Matej',
  'Sculac, T'       : 'Šćulac, Toni',
  'Giljanovic, D'   : 'Giljanović, Duje',
  'Ceci, S'         : 'Ceci, Saša',
  'Majumder, D'     : 'Majumder, Devdatta',
  'Mishra, S'       : 'Mishra, Saswat'
}

journals = {
  'JHEP'              : 'The Journal of high energy physics',
  'Phys. Rev. Lett.'  : 'Physical review letters',
  'Eur. Phys. J. C'   : 'European physical journal C : particles and fields',
  'JINST'             : 'Journal of Instrumentation',
  'Phys. Lett. B'     : 'Physics letters. B',
  'Phys. Rev. C'      : 'Physical review. C',
  'Phys. Rev. D'      : 'Physical review. D',
  'Nature Phys.'      : 'Nature physics',
  'Comput. Softw. Big Sci.' : 'Computing and software for big science',
  'Nucl. Instrum. Meth. A' : 'Nuclear instruments & methods in physics research. Section A, Accelerators, spectrometers, detectors and associated equipment'
}

issn = {
  'JHEP'              : ['1126-6708', '1029-8479'],
  'Phys. Rev. Lett.'  : ['0031-9007', '1079-7114'],
  'Eur. Phys. J. C'   : ['1434-6044', '1434-6052'],
  'JINST'             : ['1748-0221', '1748-0221'],
  'Phys. Lett. B'     : ['0370-2693', '1873-2445'],
  'Nature Phys.'      : ['1745-2473', '1745-2481'],
  'Phys. Rev. C'      : ['2469-9985', '2469-9993'],
  'Phys. Rev. D'      : ['2470-0010', '2470-0029'],
  'Comput. Softw. Big Sci.' : ['2510-2036', '2510-2044'],
  'Nucl. Instrum. Meth. A'  : ['0168-9002', '1872-9576']
}

input_file = 'list_of_papers.bib'
output_file = 'list_of_CROSBI_input.json'
list_of_CROSBI_input = output_file
list_from_CROSBI = 'list_from_CROSBI.bib'

# --------------------------------------------------
