import os
import sys
import json
import time
import bibtexparser

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from optparse import OptionParser

import _configuration as cfg
from _1_prepare_input import get_list_of_papers


# --------------------------------------------------
# Configuration

list_of_CROSBI_input = cfg.list_of_CROSBI_input
list_from_CROSBI     = cfg.list_from_CROSBI
keywords             = cfg.keywords
collaboration_name   = cfg.collaboration_name
username             = cfg.username
password             = cfg.password

# --------------------------------------------------

base_dir = os.path.dirname(os.path.realpath(__file__))


def get_input_for_CROSBI(list_of_CROSBI_input):

  with open(list_of_CROSBI_input) as f:
    return json.load(f)

def upload_to_CROSBI(dict_papers, username, password, papers_list, dry_run=False):

  browser = webdriver.Firefox()
  browser.get("http://bib.irb.hr/upis-pocetak")
  browser.implicitly_wait(3)

  # Login
  username_element = browser.find_element_by_id("username")
  password_element = browser.find_element_by_id("password")
  username_element.send_keys(username)
  password_element.send_keys(password)
  browser.find_element_by_name('Submit').click()
  browser.implicitly_wait(5)

  # Get list of DOIs for already uploaded papers
  list_of_papers = get_list_of_papers(list_from_CROSBI)
  list_of_DOIs = []

  for _p in list_of_papers.entries:
    list_of_DOIs.append( _p['doi'].lower() )

  uploaded_DOIs = open('uploaded_DOIs.txt', 'a+')

  # Loop over all papers
  for _n, (_d, _info) in enumerate(dict_papers.items(), 1):

    skip = True

    if papers_list:
        papers = papers_list.split(',')
        for p in papers:
            if '-' in p:
                p_range = p.split('-')
                p_low, p_high = [p_range[i] for i in (0, 1)]
                if p_high != '':
                    if int(_n) >= int(p_low) and int(_n) <= int(p_high):
                        skip = False
                        break
                else:
                    if int(_n) >= int(p_low):
                        skip = False
                        break
            else:
                if int(_n) == int(p):
                    skip = False
                    break
    else:
        skip = False

    if skip:
        continue

    print('------------------------------------------------')
    print('Paper:', _n)
    print('\nDOI:', _d)
    print('Title:', _info['naslov'])

    if _info['doi'].lower() in list_of_DOIs:
      print('  Paper already in CROSBI')
      continue
    else:
      print('  Paper not in CROSBI')

    # Go to the first page
    browser.get('http://bib.irb.hr/upis-pocetak')
    browser.implicitly_wait(3)
    browser.find_element_by_name('novirad').click()

    # Izaberite kategoriju rada
    browser.find_element_by_xpath('//select[@name="kategorija"]/option[@value="Znanstveni"]').click()

    # URL rada u otvorenom pristupu
    url_element = browser.find_element_by_name('urloa')
    url_element.send_keys(_info['url'])

    # DOI rada
    doi_element = browser.find_element_by_name('doi')
    doi_element.send_keys(_info['doi'])

    # Odaberite vrstu recenzije
    browser.find_element_by_xpath('//select[@name="vrst_recenzije"]/option[@value="Recenzija"]').click()

    # Autori
    autori_element = browser.find_element_by_name('autori')
    autori_element.send_keys(_info['autori'])

    browser.find_element_by_xpath('//select[@name="suradnja_medjunarodna"]/option[@value="DA"]').click()

    # Ime kolaboracije
    autori_element = browser.find_element_by_name('kolaboracija')
    autori_element.send_keys(collaboration_name)

    # Naslov rada na izvornom jeziku (na kojem je rad napisan)
    naslov_element = browser.find_element_by_name('naslov')
    naslov_element.send_keys(_info['naslov'])

    # Naslov rada na engleskom jeziku ("trazi poseban tretman"-Jelena)
    title_element = browser.find_element_by_xpath("//div[@class='topdiv']//textarea[@name='title']")
    title_element.send_keys(_info['naslov'])

    # Godina
    godina_element = browser.find_element_by_name('godina')
    godina_element.send_keys(_info['godina'])

    # Puni naziv casopisa
    casopis_element = browser.find_element_by_name('casopis')
    casopis_element.send_keys(_info['casopis'])

    # ISSN
    issn_element = browser.find_element_by_name('issn')
    issn_element.send_keys(_info['issn'])

    issn_e_element = browser.find_element_by_name('issn_e')
    issn_e_element.send_keys(_info['e-issn'])

    # Volumen
    volumen_element = browser.find_element_by_name('volumen')
    volumen_element.send_keys(_info['volumen'])

    # Broj
    if _info['broj'] != '':
        broj_element = browser.find_element_by_name('broj')
        broj_element.send_keys(_info['broj'])

    if _info['stranica_prva'] != '' and _info['stranica_zadnja'] != '':
        # Pocetna stranica rada
        pocetna_stranica_element = browser.find_element_by_name('stranica_prva')
        pocetna_stranica_element.send_keys(_info['stranica_prva'])

        # Zadnja stranica rada
        zadnja_stranica_element = browser.find_element_by_name('stranica_zadnja')
        zadnja_stranica_element.send_keys(_info['stranica_zadnja'])
    else:
        # Broj rada
        broj_rada_element = browser.find_element_by_name('broj_rada')
        broj_rada_element.send_keys(_info['broj_rada'])

        # ukupno_stranica
        ukupno_stranica_element = browser.find_element_by_name('ukupno_stranica')
        ukupno_stranica_element.send_keys(_info['ukupno_stranica'])

    # Kljucne rijeci na izvornom jeziku rada
    kljucne_rijeci_element = browser.find_element_by_name('kljucne_rijeci')
    kljucne_rijeci_element.send_keys(_info['kljucne_rijeci'])

    # Kljucne rijeci na engleskom jeziku
    kljucne_rijeci_en_element = browser.find_element_by_name('key_words')
    kljucne_rijeci_en_element.send_keys(_info['kljucne_rijeci'])

    # Sazetak na izvornom jeziku rada
    sazetak_element = browser.find_element_by_name('sazetak')
    sazetak_element.send_keys(_info['sazetak'])

    # Brojevi projekata
    # Skip, Nothing for now

    # Cjeloviti tekst rada
    browser.find_element_by_name('datoteka').send_keys(os.path.join(base_dir, _info['file']))

    # Izvorni jezik rada
    browser.find_element_by_xpath('//select[@name="jezik"]/option[@value="eng"]').click()

    # Provjera upisanih elemenata
    browser.find_element_by_name(".submit").click()
    browser.implicitly_wait(60)

    # Podrucje znanosti
    browser.execute_script("$('select[name=pozn_all] option[value=\"1.02\"]').click();")

    # Odabir znanstvene ustanove
    browser.execute_script("$('select[name=ustn_all] option[value=\"98\"]').click();")
    browser.execute_script("$('select[name=ustn_all] option[value=\"23\"]').click();")
    browser.execute_script("$('select[name=ustn_all] option[value=\"177\"]').click();")

    if not dry_run:
      browser.find_element_by_name("ok").click()
      browser.implicitly_wait(2)

      uploaded_DOIs.write(_info['doi'] + '\n')
    else:
      print("Press ENTER to continue")
      input()

  print('------------------------------------------------')

  browser.quit()

  uploaded_DOIs.close()


if __name__ == '__main__':
  # usage description
  usage = "Usage: python %prog [options] \nExample: python %prog -n"

  # input parameters
  parser = OptionParser(usage=usage)

  parser.add_option("-n", "--dry_run", dest="dry_run", action="store_true",
                      help="Perform a dry run with no uploads made",
                      default=False)

  parser.add_option("-p", "--papers", dest="papers",
                      help="Comma-separated list of papers or paper ranges",
                      metavar="PAPERS")

  (options, args) = parser.parse_args()

  if options.dry_run:
    print("Running in a dry run mode. No uploads will be made")

  # Open list of CROSBI input
  input_for_CROSBI = get_input_for_CROSBI(list_of_CROSBI_input)

  # Upload
  upload_to_CROSBI(input_for_CROSBI, username, password, options.papers, options.dry_run)
